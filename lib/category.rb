require 'yaml'

module Gitlab
  module Homepage
    class Category
      attr_reader :key

      def initialize(key, data)
        @key = key
        @data = data
      end

      # rubocop:disable Style/MethodMissingSuper
      # rubocop:disable Style/MissingRespondToMissing

      ##
      # Middeman Data File objects compatibiltiy
      #
      def method_missing(name, *args, &block)
        @data[name.to_s]
      end

      # rubocop:enable Style/MethodMissingSuper
      # rubocop:enable Style/MissingRespondToMissing

      def features?
        !features.empty?
      end

      def features
        @features ||= Feature.for_category(self)
      end

      def missing_features?
        !missing_features.empty?
      end

      def missing_features
        @missing_features ||= features.dup.keep_if(&:missing?)
      end

      def self.all!
        @categories ||= YAML.load_file('data/categories.yml')
        @categories.map do |key, data|
          new(key, data)
        end
      end

      def self.for_stage(stage)
        all!.dup.keep_if { |category| category.stage == stage.key }
      end
    end
  end
end
