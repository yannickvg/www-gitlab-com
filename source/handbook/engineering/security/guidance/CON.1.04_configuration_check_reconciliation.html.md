---
layout: markdown_page
title: "CON.1.04 - Configuration Check Reconciliation: CMDB Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CON.1.04 - Configuration Check Reconciliation: CMDB

## Control Statement

GitLab reconciles the established device inventory against the enterprise log repository quarterly; devices which do not forward security configurations are remediated.

## Context

This control helps to close the loop between device inventory information and production logs. If all production systems are sending the appropriate logs, there should be a parity between the device inventory GitLab collects and the logs generated from those systems. This control is meant to be a check on the "Configuration Check" control. This reconciliation ensures that all systems that should be forwarding security configuration information, are.

## Scope

This control applies to all production systems.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CON.1.04_configuration_check_reconciliation.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CON.1.04_configuration_check_reconciliation.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CON.1.04_configuration_check_reconciliation.md).

## Framework Mapping

* SOC2
  * CC6.1
